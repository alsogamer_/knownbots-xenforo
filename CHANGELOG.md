CHANGELOG
=========

2.3.0 (2019-11-18)
-------------------

* added curl as a robot
* show robots online in Members online and Online statistics widgets 

2.2.0 (2019-09-30)
-------------------

* merge new bot array with core array rather than clobbering it

2.1.0 (2019-09-30)
-------------------

* new bots added
* added tool to admin area to show list of bots

2.0.0a (2018-08-12)
-------------------

* re-release with new addon_id

2.0.0 (2018-06-11)
------------------

* migrate to XF2

1.0.1 (2018-02-21)
------------------

* new bots
* new bots
* lots of new bots
* added function so we can get robot map for display in admin tools
* keys need to be lower case
* added crawler4j; linguee; linkpadbot; mixrankbot; nlnz_iaharvester; ouclicksbot; semrushbot; seokicks-robot; 
  sitesucker; statuscase/virusscanner; vegi bot; wotbox
* added BingLocalSearch
* added LTX71

1.0.0 (2016-07-11)
------------------

* first working version
