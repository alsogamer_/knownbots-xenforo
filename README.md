Known Bots for XenForo 2.x
==========================

This XenForo 2.x addon adds additional definitions for bot detection in sessions.

By [Simon Hampel](https://twitter.com/SimonHampel).

Requirements
------------

This addon requires PHP 5.4 or higher and works on XenForo 2.x

Usage
-----

When you look at Current Visitors, you'll see additional robots identified - also look at the "Robots" list on that page
 http://www.example.com/community/online/?type=robot
 
There is also a "Show list of Known Bots" menu in the admin area Tools section which shows the bots we have defined and
links to more information about them (where available)

A count of robots online is added to the Members online and Online statistics widgets